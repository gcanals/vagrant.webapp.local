# Machine virtuelle pour le dev. web / vagrant
## pour installation sur machine personnelle


###Fabriqué avec PuPHPet : https://puphpet.com/


### Prérequis :
* machine linux, macOS ou windows (sauf win10 !)

* VirtualBox (4.3.x) et Vagrant (>1.7.4) doivent être installés sur votre machine.

### caractéristiques de la machine virtuelle : 
* host ip : 192.168.56.12
* hostname : webapp.local

* **système** : ubuntu1404-x64 / php 5.5

* **apache** :
   * **vhost défaut** : webapp.local
   * **docroot** : /var/www/html -> ./html
   	
	* **vhost pour le dev** : mywebapp.local www.mywebapp.local
	* **docroot** :   /var/www/ (-> ./)

* **mysql** : 
	* 5.5 - root/123
	* **accès** : webetu.local/adminer
	* acès depuis la machine hôte avec Sequel Pro ou Mysql Workbench : 
		* connexion avec un tunnel ssh, 
			* username : `vagrant`
			* ssh key : `puphpet/files/dot/ssh/id_rsa` (clé générée **après** démarrage de la machine)
		  
* **php** : 5.5 - config dev / xdebug
* **mongodb**: installé avec php-mongo
* **redis** : installé avec php_redis
* **sqlite** : installé avec php_sqlite

### installation, création, démarrage de la machine

* dans votre répertoire de développement, cloner le dépôt :
~~~
git clone https://gcanals@bitbucket.org/gcanals/vagrant.webapp.local.git
~~~

* dans le répertoire créé, créer et démarrer la machine virtuelle avec vagrant : 
~~~
$vagrant up  
~~~

* si tout se passe bien la machine est créée et lancée

### utilisation

* déclarer les hosts dans le fichier /etc/hosts (ou \System32\drivers\etc\hosts ) en ajoutant la ligne : 
~~~
192.168.56.112   webapp.local mywebapp.local www.mywebapp.local 
~~~

* modifier la configuration réseau de votre navigateur pour éviter d'utiliser le proxy sur webapp.local et *.mywebapp.local

* tester : l'url mywebapp.local/info.php doit retourner la configuration php

 
### ajouter des hôtes virtuels
pour ajouter un hôte virtuel, utiliser le fichier de base fournit dans le répertoire vhosts et adpatez une copie à vos besoins.

Démarrer la machine virtuelle si elle n'est pas active, puis connectez vous : 
~~~
vagrant up
vagrant ssh
~~~

dans la machine, copiez votre fichier de définition du vhost dans le réportoire /etc/apache2/sites-available, puis activez le vhost : 
~~~
sudo cp /var/www/vhosts/mon_vhost.conf /etc/apache2/sites-available
sudo a2ensite mon_vhost
~~~

Déconnectez vous de la machine, puis redémarrez la :
~~~
vagrant reload
~~~

N'oubliez pas de déclarer votre vhost dans votre fichier /etc/hosts
